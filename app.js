var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('public'))


const userRouter = require('./routes/users.js');
app.use('/users',userRouter);
const homeRouter = require('./routes/index.js');
app.use(homeRouter);
const adduserRouter = require('./routes/adduser.js');
app.use(adduserRouter);


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.listen(8000,function(){
  console.log(console.log('server started ...'))
})

const url = 'mongodb://localhost/UserDB';
mongoose.connect(url,{useNewUrlParser:true})
const con = mongoose.connection;

con.on('open', function(){
    console.log('connected...');
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

module.exports = app;

