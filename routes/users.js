const express = require('express');
const router = express.Router();
//import model here
const User = require('../models/user');

router.get('/', async(req, res)=>{
    try{
        const userslist = await User.find();
        res.render('users', {title:'Main page', records:userslist});
    }catch(err){
        res.send(`error  ${err}`);
    }
})


router.get('/:id', (req, res)=>{
    try{
        const user = User.findById(req.params.id);
        res.json(user);
    }catch(err){
        res.send(`error  ${err}`);
    }
})


router.post('/', async(req,res)=>{
    const user = new User({
        userid:req.body.userId,
        name:req.body.name,
        course:req.body.course,
        sub:(req.body.sub == 'on')
    })
    try{
        const u1= await user.save();
        res.json(u1);
    }catch(err){
        res.send(err);
    }
});


router.patch('/:id', async(req,res)=>{
    try{
        const user = await User.findById(req.params.id);
        user.sub = req.body.sub;
        const u1 = await user.save();
        res.json(u1);
    }catch(err){
        res.send(err);
    }
});

router.put('/:id', async(req,res)=>{
    try{
        const user = await User.findById(req.params.id);
        user.sub = req.body.sub;
        const u1 = await user.save();
        res.json(u1);
    }catch(err){
        res.send(err);
    }
});

router.delete('/:id', async(req,res,next)=>{
    User.remove({_id:req.params.id})
    .then(result=>{
        res.status(200).json({
            message:'product deleted',
            result:result
        })
    .catch(err=>{
        res.status(500).json({
            error:err
            })
        })
    })
});


router.put('/:id', async(req,res)=>{
    User.findOneAndUpdate({_id:req.params.id},
        {$set:{
            userid:req.body.userid,
            name:req.body.name,
            course:req.body.course,
            sub:req.body.sub
        }
    
    })
    .then(result=>{
        res.status(200).json({
            message:'product deleted',
            result:result
        })
    .catch(err=>{
        console.log(err);
        })
    })
});

module.exports = router;

// router.delete('/:id', async(req,res)=>{
//     try{
//         const user = await User.findById(req.params.id);
//         const u1 = await user.remove();
//         res.json(u1);
//     }catch(err){
//         res.send(err);
//     }
// })


