var express = require('express');
var router = express.Router();

router.get('/add-user', function(req, res, next) {
  res.render('adduser', {title:'Main page'});
});

module.exports = router;