//define schema(table structure) for user
const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
    userid:{
        type: Number,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    course:{
        type: String,
        required: true
    },
    sub:{
        type: Boolean,
        required: true,
        default: false
    }

})

module.exports = mongoose.model('User', userSchema); 